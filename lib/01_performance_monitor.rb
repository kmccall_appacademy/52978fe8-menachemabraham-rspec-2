def measure(times = 1, &prc)
    before = Time.now
    times.times(&prc)
    (Time.now - before) / times
end
