def reverser(&prc)
  words = prc.call.split
  words.map(&:reverse).join(" ")
end

def adder(to_add = 1, &prc)
  prc.call + to_add
end

def repeater(times = 1)
  times.times { yield }
end
